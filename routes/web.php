<?php

use Illuminate\Support\Facades\Route;
Use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/login',[MainController::class,'login'])->name('login');
Route::get('/register',[MainController::class,'register'])->name('register');
Route::post('/save',[MainController::class,'save'])->name('save');
Route::post('/logincheck',[MainController::class,'loginCheck'])->name('loginCheck');

Route::group(['middleware'=>'auth:web'],function(){
    Route::get('/dashboard',[MainController::class,'dashboard'])->name('dashboard');
    Route::get('/logout',[MainController::class,'logout'])->name('logout');
        

});
+++
