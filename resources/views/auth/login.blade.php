<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,,height=device-height,initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
         integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" 
         crossorigin="anonymous">
         <title>Login</title>
    </head>
    <body>
       <div class="container" >
           <div class="row" style="margin-top:45px;margin-left:350px;">
                <div class="col-md-6 col-md-offset-2">
                    <h4 class="text-center">Login Form</h4><hr>
                    <form action="{{url('logincheck')}}" method="POST">

                    @if(Session::get('fail'))
                        <div class="alert alert-danger">
                            <li>{{Session::get('fail')}}</li>
                        </div>
                        @endif

                        @csrf
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" placeholder="Enter email address" value="{{old('email')}}">
                            <span class="text-danger">@error('email'){{ $message }} @enderror</span>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Enter password" value="{{old('password')}}">
                            <span class="text-danger">@error('password'){{ $message }} @enderror</span>
                        </div>
                        <button type="submit" class="btn btn-block btn-primary">Sign In</button><br>
                        <a href="{{url('register')}}">I don't have an account,create new one</a>
                    </form>
                </div>
           </div>
       </div>
    </body>
</html>