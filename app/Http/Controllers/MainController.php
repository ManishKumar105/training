<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

use Illuminate\Support\Facades\Hash;
use Auth;
use Session;

class MainController extends Controller
{
    public function login()
    {
        if(Auth::check())
        {
            return redirect('dashboard');
        }
        return view('auth.login');
    }
    public function register()
    {
        return view('auth.register');
    }
    
    public function save(Request $request)
    {
        $request->validate([
            "employee_id"=>"required",
            "name"=>"required",
            "phone"=>"required",
            "email"=>"required|email|unique:users",
            "password"=>"required|min:5|max:12",
            "user_type"=>"required"
        ]);
        $register = new User;
        $register->employee_id = $request->employee_id;
        $register->name = $request->name;
        $register->phone = $request->phone;
        $register->email = $request->email;
        $register->password = Hash::make($request->password);
        $register->user_type = $request->user_type;
        $register->status = 'Active';
        
        $result = $register->save();
        if($result)
        {
            return back()->with('success','New User has been successfully added to database');
        }else
        {
            return back()->with('fail','Something went wrong, try again later');
        }
    }
    public function loginCheck(Request $request)
    {
        $request->validate([
            "email"=>"required|email",
            "password"=>"required|min:5|max:12"
        ]);
       
         $registers = User::where('email','=',$request->email)->first();
        if(!$registers)
        {
            return back()->with('fail','We do not recognize your email address');
        }else
        {
            if(Auth::attempt(['email'=>$request->email,'password'=>$request->password,'status'=>'Active']))
            {
                return redirect('dashboard');
            }else
            {
                return back()->with('fail','Incorrect Password');
            }
        }
        }
    
    public function dashboard()
    {
        return view('dashboard');
    }
    
    public function logout()
    {
        Auth::logout();
        return redirect('login');
     
    }
}
